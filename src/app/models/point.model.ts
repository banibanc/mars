export enum Point {
    North = 'NORTH',
    East = 'EAST',
    South = 'SOUTH',
    West = 'WEST'
}