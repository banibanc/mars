export enum Command {
    Move = 'MOVE',
    Left = 'LEFT',
    Place = 'PLACE',
    Report = 'REPORT',
    Right = 'RIGHT',
    Unknown = 'UNKNOWN'
}