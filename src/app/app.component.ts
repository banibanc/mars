import { Component, OnInit } from '@angular/core';
import { Command } from './models/command.model';
import { Point } from './models/point.model';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    instruction: any;
    error: string;
    rover: {x: number, y: number, f: string};
    points: Array<string>;
    commands: Array<string>;
    limit: number;
    initial: boolean;

    ngOnInit() {
        this.rover = null;
        this.error = '';
        this.instruction = null;
        this.points = [Point.North, Point.East, Point.South, Point.West];
        this.commands = [Command.Move, Command.Left, Command.Place, Command.Report, Command.Right];
        this.limit = 4;
        this.initial = true;
    }

    onInstruction(instruction: any) {
        this.instruction = instruction;
        let commandValue = Command.Unknown;

        if (this.initial === true && this.instruction.startsWith('PLACE') === false) {
            this.errorLog('sequence has to be started by a PLACE command');
            return false;
        } else {
            this.initial = false;
        }

        Object.keys(Command).forEach((enumKey) => {
            if (this.instruction.startsWith(Command[enumKey]) === true) {
                commandValue = Command[enumKey];
            }
        });

        if (commandValue !== Command.Unknown) {
            if (commandValue === Command.Move) {
                this.forward();
            }
            if (commandValue === Command.Left || commandValue === Command.Right) {
                this.turn(commandValue);
                this.dismissError();
            }
            if (commandValue === Command.Place) {
                const coords = this.instruction.replace(`${Command.Place} `, '').split(',');
                this.place(coords);
            }
            if (commandValue === Command.Report) {
                this.errorLog('I\'m at x :' + this.rover.x + ', y : ' + this.rover.y + ', facing : ' + this.rover.f);
            }
        } else {
            this.errorLog('Unknown command : you must use "MOVE", "REPORT", "LEFT", "RIGHT" OR "PLACE X,Y,F"');
        }
    }

    dismissError() {
        this.error = '';
    }

    onError(error: string) {
        this.error = error;
    }

    forward() {
        if (this.rover.f === Point.North) {
            if (this.checkLimit(this.rover.y + 1)) {
                this.rover.y++;
                this.dismissError();
            } else {
                this.errorLog('cannot do that !');
            }
        }
        if (this.rover.f === Point.South) {
            if (this.checkLimit(this.rover.y - 1)) {
                this.rover.y--;
                this.dismissError();
            } else {
                this.errorLog('cannot do that !');
            }
        }
        if (this.rover.f === Point.East) {
            if (this.checkLimit(this.rover.x + 1)) {
                this.rover.x++;
                this.dismissError();
            } else {
                this.errorLog('cannot do that !');
            }
        }
        if (this.rover.f === Point.West) {
            if (this.checkLimit(this.rover.x - 1)) {
                this.rover.x--;
                this.dismissError();
            } else {
                this.errorLog('cannot do that !');
            }
        }
    }

    turn(direction: Command.Left | Command.Right) {
        const currentIndex = this.points.indexOf(this.rover.f);
        if (direction === Command.Left) {
            if (currentIndex - 1 < 0) {
                this.rover.f = this.points[3];
            } else {
                this.rover.f = this.points[currentIndex - 1];
            }
        } else if (direction === Command.Right) {
            if (currentIndex + 1 > 3) {
                this.rover.f = this.points[0];
            } else {
                this.rover.f = this.points[currentIndex + 1];
            }
        }
    }

    place(coords: Array<any>) {
        if (this.checkLimit(coords[0]) && this.checkLimit(coords[1]) && this.points.indexOf(coords[2]) !== -1) {
            this.rover = {x: Number(coords[0]), y: Number(coords[1]), f: coords[2]};
            this.dismissError();
        } else {
            this.errorLog('Enter correct coordinates');
        }
    }

    checkLimit(newValue: number) {
        if (newValue > this.limit || newValue < 0) {
            return false;
        } else {
            return true;
        }
    }

    errorLog(msg: string) {
        this.error = msg;
    }
}
