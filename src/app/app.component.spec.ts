import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { TableComponent } from './components/table/table.component';
import { CommandComponent } from './components/command/command.component';

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                RouterTestingModule,
            ],
            declarations: [
                AppComponent,
                TableComponent,
                CommandComponent
            ],
        }).compileComponents();
    }));

    it('should create the app', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});

describe('Command', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent,
                TableComponent,
                CommandComponent
            ],
            imports: [
                FormsModule,
                RouterTestingModule,
            ]
        }).compileComponents();
    }));

    it('First instruction should be PLACE x,x,x', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
        fixture.detectChanges();
        expect(app.rover).toBe(null, 'null at first');
        app.onInstruction('MOVE');
        expect(app.error).toBe('sequence has to be started by a PLACE command', 'still null cause first instruction wasn\'t the rignt one');
        app.onInstruction('PLACE 1,2,EAST');
        expect(app.rover).toEqual({x: 1, y: 2, f: 'EAST'}, 'rover placed because of correct first instruction');
    });

    it('Cannot leave the table', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
        fixture.detectChanges();
        app.onInstruction('PLACE 3,' + app.limit.toString() + ',NORTH');
        app.onInstruction('MOVE');
        expect(app.rover).toEqual({x: 3, y: app.limit, f: 'NORTH'}, 'rover did not move to avoid fall');
        expect(app.error).toBe('cannot do that !', 'Error shown');
    });

    it('Cannot spawn out of the table', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
        fixture.detectChanges();
        app.onInstruction('PLACE ' + (app.limit + 1).toString() + ',4,NORTH');
        expect(app.rover).toBe(null, 'rover did not spawn to avoid fall');
        expect(app.error).toBe('Enter correct coordinates', 'Error shown');
    });

    it('Must use one of the commands', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
        fixture.detectChanges();
        app.onInstruction('PLACE 1,2,NORTH');
        app.onInstruction('forward');
        expect(app.error).toBe('Unknown command : you must use "MOVE", "REPORT", "LEFT", "RIGHT" OR "PLACE X,Y,F"', 'Error shown');
    });

    it('Must use one of the points for direction', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
        fixture.detectChanges();
        app.onInstruction('PLACE 1,2,NORTHz');
        expect(app.error).toBe('Enter correct coordinates', 'Error shown');
    });

    it('Should report', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
        fixture.detectChanges();
        app.onInstruction('PLACE 1,4,NORTH');
        app.onInstruction('REPORT');
        expect(app.error).toBe('I\'m at x :1, y : 4, facing : NORTH', 'Report shown');
    });
});
