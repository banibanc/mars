import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
    width: Array<number>;
    height: Array<number>;
    @Input() rover: {x: number, y: number, f: string};
    @Input() limit: number;
    @Output() log: EventEmitter<any> = new EventEmitter();
    constructor() { }

    ngOnInit() {
        this.width = [];
        this.height = [];
        for (let i = 0; i <= this.limit; i++) {
            this.width.push(i);
        }
        for (let i = this.limit; i >= 0; i--) {
            this.height.push(i);
        }
    }
}
