import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-command',
    templateUrl: './command.component.html',
    styleUrls: ['./command.component.css']
})
export class CommandComponent implements OnInit {
    cli: string;
    @Output() instruction: EventEmitter<any> = new EventEmitter();
    constructor() { }

    ngOnInit() {
        this.cli = '';
    }

    execute() {
        this.instruction.emit(this.cli);
        this.cli = '';
    }
}
